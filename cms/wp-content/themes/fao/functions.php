<?php
// TODO agregar screenshot.png logo fao
// TODO agregar logo fao en login

add_theme_support( 'post-thumbnails' );
// NEWS IMAGES SIZES
add_image_size( 'main_header', 1600, 800 , true);
add_image_size( 'card_cover', 330, 260 , true);
add_image_size( 'card_category_cover', 435, 290 , true);

//add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter( 'allowed_block_types', 'theme_allowed_block_types' );
  
function theme_allowed_block_types( $allowed_blocks ) {
  
    return array(
        'core/paragraph',
        'core/heading',
        'core/image',
        'core/list',
        'core/separator',
        'core/media-text',
        'core/audio',
        'core/video',
        'core/embed'
    );
  
}

function my_theme_deny_list_blocks() {
    wp_enqueue_script(
        'deny-list-blocks',
        get_template_directory_uri() . '/deny-list-blocks.js',
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' )
    );
}
add_action( 'enqueue_block_editor_assets', 'my_theme_deny_list_blocks' );

function my_custom_login_logo() {
	echo '<style  type="text/css"> h1 a {  background-image:url(' . get_stylesheet_directory_uri()  . '/assets/img/logo_exoticas_blanco.png)  !important; width: 200px !important; background-size: contain !important; filter: drop-shadow(2px 4px 6px black);}</style>';
}
add_action('login_head',  'my_custom_login_logo');

/**********************
**** API ENDPOINTS ****
**********************/

add_action('rest_api_init', function () {
	register_rest_route( 'wp/v1', 'home',array(
		'methods'  => 'GET',
		'callback' => 'get_home_api'
	));
	register_rest_route( 'wp/v1', 'nav',array(
		'methods'  => 'GET',
		'callback' => 'get_nav_api'
	));
	register_rest_route( 'wp/v1', 'content/(?P<post_id>\d+)',array(
		'methods'  => 'GET',
		'callback' => 'get_content_api'
	));
	register_rest_route( 'wp/v1', 'category/(?P<category_id>\d+)',array(
		'methods'  => 'GET',
		'callback' => 'get_category_api'
	));
	register_rest_route( 'wp/v1', 'search',array(
		'methods'  => 'GET',
		'callback' => 'get_search_api'
	));
});


/**********************
**** API FUNCTIONS ****
**********************/

function get_home_api() {
	$sections = [];
	$sections_field = get_field('seccion', 167);

	foreach($sections_field as $section){
		if($section['tipo'] == 'gallery'){
			$sections[] = slider_home_posts($section);
		}
		if($section['tipo'] == 'link'){
			$item['type'] = "category";
			$item['title'] = $section['categoria']->name;
			$item['link'] = '/categoria/'.$section['categoria']->term_id.'/'.$section['categoria']->slug;
			$item['poster'] = normalize_img( get_field('portada', 'category_'.$section['categoria']->term_id) );
			$sections[] = $item;
		}
	}

	$response = new WP_REST_Response($sections);
    $response->set_status(200);

	return $response;
}

function get_nav_api() {
	$redes = get_field('redes_sociales', 167);
	
	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC',
		'hide_empty' => 1,    
    	'exclude' =>array(1) // desire id
	) );
	
	if ( !$categories ) {
		return new WP_Error( 'no_nav', 'No hay menu', array('status' => 404) );
	}

	foreach($categories as $item){
		$item_cat['title'] = $item->name;
		$item_cat['count'] = $item->count;
		$item_cat['link'] = '/categoria/'.$item->term_id.'/'.$item->slug;
		$nav['items'][] = $item_cat;
	}

	$nav['redes'] = $redes;

	$response = new WP_REST_Response($nav);
    $response->set_status(200);

	return $response;
}

function get_search_api($request) {
	$param = $request->get_param( 'search' );
	$args = array('s' => $param);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		
		while ( $query->have_posts() ) {
			$query->the_post();

			$post['title'] = $query->post->post_title;
			$post['id'] = $query->post->ID;
			$post['slug'] = $query->post->post_name;
			$post['description'] = $query->post->post_excerpt;
			$post['link'] = '/contenido/'.$query->post->ID.'/'.$query->post->post_name;
			$post['poster'] = normalize_img( get_field('portada',$query->post->ID) );

			$search['cards'][] = $post;
		}
	}else{
		return new WP_Error( 'no_search_posts', 'No hay contenido para esta categoría', array('status' => 404) );
	}

	$number = count($search['cards']);

	$search['title'] = $param;
	$search['description'] = 'Se encontraron '.$number.' resultados para su búsqueda';
	$search['type'] = "search";

	$response = new WP_REST_Response($search);
    $response->set_status(200);

	return $response;
}

function get_category_api($request) {
	$category_posts = get_posts(array('category' => $request['category_id']));

	if (empty($category_posts)) {
		return new WP_Error( 'no_category_posts', 'No hay contenido para esta categoría', array('status' => 404) );
	}

	$category['title'] = get_cat_name($request['category_id']);
	$category['description'] = strip_tags(category_description($request['category_id']));
	$category['type'] = "category";
	$category['poster'] = normalize_img( get_field('portada', 'category_'.$request['category_id']) );

	foreach($category_posts as $data):
		$category['cards'][] = normalize_post($data);
	endforeach;

	$response = new WP_REST_Response($category);
    $response->set_status(200);

	return $response;
}

function get_content_api($request) {
	$wp_post = get_post($request['post_id']); 

	if (empty($wp_post)) {
		return new WP_Error( 'no_content', 'No hay contenido con este ID', array('status' => 404) );
	}

	$content = apply_filters('the_content', $wp_post->post_content);

	$post['title'] = $wp_post->post_title;
	$post['content'] = $content;
	$post['slug'] = $wp_post->post_name;
	$post['description'] = $wp_post->post_excerpt;
	$post['poster'] = normalize_img( get_field('portada',$request['post_id']) );
	$post['subtitle'] = get_field('subtitulo',$request['post_id']) ;
	$post['time'] = get_field('duracion',$request['post_id']);

	$cat =  get_the_category($request['post_id']);
	$related = get_posts(array('category' => $cat[0]->term_id, 'post__not_in' => array($request['post_id']),));
	if($related):
		$post['related']['title'] = 'Contenido relacionado';
		foreach($related as  $data):
			$post['related']['cards'][] = normalize_post($data);
		endforeach;
	endif;

	$response = new WP_REST_Response($post);
    $response->set_status(200);

    return $response;
}

/****************
**** METHODS ****
*****************/

function normalize_post($post){
	$item['title'] = $post->post_title;
	$item['id'] = $post->ID;
	$item['slug'] = $post->post_name;
	$item['description'] = $post->post_excerpt;
	$item['time'] = get_field('duracion',$post->ID);
	$item['link'] = '/contenido/'.$post->ID.'/'.$post->post_name;
	$item['poster'] = normalize_img( get_field('portada',$post->ID) );

	return $item;
}

function normalize_img($img){
	$new_img['sizes']['thumbnail'] = $img['sizes']['thumbnail'];
	$new_img['sizes']['medium'] = $img['sizes']['medium'];
	$new_img['sizes']['large'] = $img['sizes']['large'];
	$new_img['sizes']['main_header'] = $img['sizes']['main_header'];
	$new_img['sizes']['card_cover'] = $img['sizes']['card_cover'];
	$new_img['sizes']['card_category_cover'] = $img['sizes']['card_category_cover'];
	$new_img['url'] = $img['url'];
	$new_img['title'] = $img['title'];

	return $new_img;
}

function slider_home_posts($cat){
	$posts = get_posts(array('category' => $cat['categoria']->term_id));
	// $catObj = get_term_by('id', $cat, 'category');
	
	$section['type'] = "slider";
	$section['title'] = $cat['categoria']->name;
	$section['link'] = '/categoria/'.$cat['categoria']->term_id.'/'.$cat['categoria']->slug;
	$section['poster'] = normalize_img( get_field('portada', 'category_'.$cat['categoria']->term_id) );

	foreach($posts as $data):
		$section['cards'][] = normalize_post($data);
	endforeach;	

	return $section;
}
?>